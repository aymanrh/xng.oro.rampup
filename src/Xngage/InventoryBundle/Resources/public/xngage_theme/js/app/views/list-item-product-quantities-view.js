define(function(require) {
    'use strict';

    const BaseView = require('oroui/js/app/views/base/view');
    const ElementsHelper = require('orofrontend/js/app/elements-helper');
    const BaseModel = require('oroui/js/app/models/base/model');
    const _ = require('underscore');

    const ListItemProductQuantitiesView = BaseView.extend(_.extend({}, ElementsHelper, {
        elements: {
            quantity: '[data-name="stock-qty"]',
            status: '[data-name="stock-status"]',
        },

        defaultOptions: {},

        modelAttr: {
            status: {},
            quantities: {}
        },

        /**
         * @inheritDoc
         */
        constructor: function ListItemProductQuantitiesView(options) {
            ListItemProductQuantitiesView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritDoc
         */
        initialize: function(options) {
            ListItemProductQuantitiesView.__super__.initialize.call(this, options);
            this.deferredInitializeCheck(options, ['productModel']);

            this.model.on('change:unit', this.render, this);
        },

        /**
         * @inheritDoc
         */
        deferredInitialize: function(options) {
            options = _.defaults(options, this.defaultOptions);
            this.initModel(options);
            if (!this.model) {
                return;
            }

            this.quantities = options.quantities;

            this.initializeElements(options);
            this.render();
        },

        /**
         * @inheritDoc
         */
        dispose: function() {
            this.disposeElements();

            if (this.model) {
                this.model.off(null, null, this);
            }

            ListItemProductQuantitiesView.__super__.dispose.call(this);
        },

        /**
         * @inheritDoc
         */
        render: function() {
            const unit          = this.model.get('unit');
            const quantities    = this.model.get('quantities');
            const stockStatus   = this.model.get('status');

            const $status       = this.getElement('status');
            const $quantity     = this.getElement('quantity');

            if (stockStatus.id !== 'in_stock') {
                $quantity.addClass('hide');
                $status.text(stockStatus.name).removeClass('hide');
            } else {
                $status.addClass('hide');
                $quantity.find('[itemprop=qty]').text(quantities[unit]);
                $quantity.removeClass('hide');
            }

            return this;
        },

        initModel: function(options) {
            this.modelAttr = _.extend({}, this.modelAttr, options.modelAttr || {});
            if (options.productModel) {
                this.model = options.productModel;
            }

            if (!this.model) {
                this.model = new BaseModel();
            }

            _.each(this.modelAttr, function(value, attribute) {
                if (!this.model.has(attribute) || !_.isEmpty(value)) {
                    this.model.set(attribute, value);
                }
            }, this);
        },
    }));

    return ListItemProductQuantitiesView;
});
