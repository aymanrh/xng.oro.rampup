<?php

namespace Xngage\InventoryBundle\EventListener;

use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\UIBundle\Event\BeforeListRenderEvent;
use Oro\Bundle\UIBundle\View\ScrollData;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Adds inventory information to the product view and edit pages.
 */
class FormViewListener
{
    /** @var TranslatorInterface */
    protected $translator;

    /** @var DoctrineHelper */
    protected $doctrineHelper;

    /** @var RequestStack */
    protected $requestStack;

    public function __construct(
        TranslatorInterface $translator,
        DoctrineHelper $doctrineHelper,
        RequestStack $requestStack
    ) {
        $this->translator = $translator;
        $this->doctrineHelper = $doctrineHelper;
        $this->requestStack = $requestStack;
    }

    public function onProductView(BeforeListRenderEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();
        if (!$request) {
            return;
        }

        $productId = (int) $request->get('id');
        if (!$productId) {
            return;
        }

        /** @var Product $product */
        $product = $this->doctrineHelper->getEntityReference('OroProductBundle:Product', $productId);
        if (!$product) {
            return;
        }

        $inventoryLevels = $this->doctrineHelper
            ->getEntityRepositoryForClass('OroInventoryBundle:InventoryLevel')
            ->findBy(['product' => $productId]);

        if (0 === count($inventoryLevels)) {
            return;
        }

        $template = $event->getEnvironment()->render(
            'XngageInventoryBundle:Product:product_inventory_view.html.twig',
            [
                'entity' => $product,
                'inventoryLevels' => $inventoryLevels,
            ]
        );
        $this->addBlock($event->getScrollData(), $template, 'xngage.inventory.product.section.inventory_options', 1900);
    }

    /**
     * @param string $html
     * @param string $label
     * @param int    $priority
     */
    protected function addBlock(ScrollData $scrollData, $html, $label, $priority)
    {
        $blockLabel = $this->translator->trans($label);
        $blockId = $scrollData->addBlock($blockLabel, $priority);
        $subBlockId = $scrollData->addSubBlock($blockId);
        $scrollData->addSubBlockData($blockId, $subBlockId, $html);
    }
}
