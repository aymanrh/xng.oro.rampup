<?php

namespace Xngage\InventoryBundle\EventListener;

use Oro\Bundle\DataGridBundle\Datasource\ResultRecord;
use Oro\Bundle\DataGridBundle\Event\PreBuild;
use Oro\Bundle\DataGridBundle\Extension\Formatter\Property\PropertyInterface;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\InventoryBundle\Provider\InventoryQuantityProviderInterface;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\SearchBundle\Datagrid\Event\SearchResultAfter;

class ProductDatagridStockListener
{
    const COLUMN_STOCK = 'stock';

    /**
     * @var DoctrineHelper
     */
    private $doctrineHelper;

    /**
     * @var InventoryQuantityProviderInterface
     */
    private $inventoryQuantityProvider;

    public function __construct(
        DoctrineHelper $doctrineHelper,
        InventoryQuantityProviderInterface $inventoryQuantityProvider
    ) {
        $this->doctrineHelper = $doctrineHelper;
        $this->inventoryQuantityProvider = $inventoryQuantityProvider;
    }

    public function onPreBuild(PreBuild $event)
    {
        $config = $event->getConfig();

        $config->offsetAddToArrayByPath(
            '[properties]',
            [
                self::COLUMN_STOCK => [
                    'type' => 'field',
                    'frontend_type' => PropertyInterface::TYPE_ROW_ARRAY,
                ],
            ]
        );
    }

    public function onResultAfter(SearchResultAfter $event)
    {
        /** @var ResultRecord[] $records */
        $records = $event->getRecords();
        $entityManager = $this->doctrineHelper->getEntityManagerForClass(Product::class);

        foreach ($records as $record) {
            /** @var Product $product */
            if ($product = $entityManager->getReference(Product::class, $record->getValue('id'))) {
                $stockArray = [];
                $stockArray['status'] = [
                    'id' => $product->getInventoryStatus()->getId(),
                    'name' => $product->getInventoryStatus()->getName(),
                ];
                foreach ($product->getAvailableUnits() as $unit) {
                    $stockArray['qty'][$unit->getCode()] = $this->inventoryQuantityProvider->getAvailableQuantity($product, $unit);
                }
                $record->addData([self::COLUMN_STOCK => $stockArray]);
            }
        }
    }
}
