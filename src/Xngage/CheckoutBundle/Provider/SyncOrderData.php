<?php

namespace Xngage\CheckoutBundle\Provider;

use Oro\Bundle\OrderBundle\Entity\Order;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;

class SyncOrderData
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * SyncOrderData constructor.
     * @param KernelInterface $kernel
     * @param Filesystem $filesystem
     */
    public function __construct(
        KernelInterface $kernel,
        Filesystem $filesystem
    ) {
        $this->kernel = $kernel;
        $this->filesystem = $filesystem;
    }

    public function pushOrderDataToErp(Order $order)
    {
        $projectDir = $this->kernel->getProjectDir();
        $erpDir = $projectDir.DIRECTORY_SEPARATOR.'erp';
        $erpOrderFile = 'order-data.txt';

        try {
            if (!$this->filesystem->exists($erpDir)) {
                $this->filesystem->mkdir($erpDir);
            }

            $erpOrderFilePath = $erpDir.DIRECTORY_SEPARATOR.$erpOrderFile;
            if (!$this->filesystem->exists($erpOrderFilePath)) {
                $this->filesystem->touch($erpOrderFilePath);
            }

            $content = "Subtotal: {$order->getSubtotal()}\n";
            $this->filesystem->appendToFile($erpOrderFilePath, $content);
        } catch (IOExceptionInterface $exception) {
            echo 'An error occurred while creating your directory at '.$exception->getPath();
        }
    }
}
