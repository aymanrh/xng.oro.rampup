<?php

namespace Xngage\CheckoutBundle\Migrations\Schema\v1_0;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\ActivityBundle\Migration\Extension\ActivityExtension;
use Oro\Bundle\ActivityBundle\Migration\Extension\ActivityExtensionAwareInterface;
use Oro\Bundle\EntityExtendBundle\EntityConfig\ExtendScope;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

class AddPickupDateAndTimeToOrder implements Migration, ActivityExtensionAwareInterface
{
    /** @var ActivityExtension */
    protected $activityExtension;

    /**
     * {@inheritDoc}
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        $table = $schema->getTable('oro_order');
        if (!$table->hasColumn('pickup_day')) {
            $table->addColumn(
                'pickup_day',
                'string',
                ['oro_options' => [
                    'extend' => [
                        'is_extend' => true,
                        'owner' => ExtendScope::OWNER_CUSTOM,
                    ],
                    'entity' => ['label' => 'Pickup day'],
                    'datagrid' => ['is_visible' => false],
                ]]
            );
        }

        if (!$table->hasColumn('pickup_time')) {
            $table->addColumn(
                'pickup_time',
                'string',
                ['oro_options' => [
                    'extend' => [
                        'is_extend' => true,
                        'owner' => ExtendScope::OWNER_CUSTOM,
                    ],
                    'entity' => ['label' => 'Pickup time'],
                    'datagrid' => ['is_visible' => false],
                ]]
            );
        }
    }


    /**
     * {@inheritDoc}
     */
    public function setActivityExtension(ActivityExtension $activityExtension)
    {
        $this->activityExtension = $activityExtension;
    }
}
