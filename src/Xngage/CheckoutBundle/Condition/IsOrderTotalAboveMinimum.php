<?php

namespace Xngage\CheckoutBundle\Condition;

use Oro\Bundle\CheckoutBundle\Entity\Checkout;
use Oro\Bundle\CheckoutBundle\Provider\CheckoutTotalsProvider;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Component\ConfigExpression\Condition\AbstractCondition;
use Oro\Component\ConfigExpression\ContextAccessorAwareInterface;
use Oro\Component\ConfigExpression\ContextAccessorAwareTrait;
use Oro\Component\ConfigExpression\Exception\InvalidArgumentException;

/**
 * Usage:.
 *
 * @is_order_total_valid:
 *      checkout: $checkout
 */
class IsOrderTotalAboveMinimum extends AbstractCondition implements ContextAccessorAwareInterface
{
    use ContextAccessorAwareTrait;

    const NAME = 'is_order_total_valid';

    private const OPTION_CHECKOUT = 'checkout';

    /**
     * @var ConfigManager
     */
    protected $configManager;

    /**
     * @var CheckoutTotalsProvider
     */
    protected $checkoutTotalsProvider;

    /**
     * @var Checkout
     */
    protected $checkout;

    /**
     * IsOrderTotalAboveMinimum constructor.
     */
    public function __construct(
        ConfigManager $configManager,
        CheckoutTotalsProvider $checkoutTotalsProvider
    ) {
        $this->configManager = $configManager;
        $this->checkoutTotalsProvider = $checkoutTotalsProvider;
    }

    /**
     * {@inheritDoc}
     */
    protected function isConditionAllowed($context)
    {
        /** @var Checkout $checkout */
        $checkout = $this->resolveValue($context, $this->checkout);
        $orderTotalArray = $this->checkoutTotalsProvider->getTotalsArray($checkout);
        $currentOrderTotal = $orderTotalArray['subtotals'][0]['amount'];
        $minOrderTotal = $this->configManager->get('xngage_checkout.min_order_total');
        if ($currentOrderTotal > $minOrderTotal) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return static::NAME;
    }

    /**
     * {@inheritDoc}
     */
    public function initialize(array $options)
    {
        if (!array_key_exists(self::OPTION_CHECKOUT, $options)) {
            throw new InvalidArgumentException(sprintf('Missing "%s" option', self::OPTION_CHECKOUT));
        }

        $this->checkout = $options[self::OPTION_CHECKOUT];

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function toArray()
    {
        return $this->convertToArray([$this->checkout]);
    }

    /**
     * {@inheritDoc}
     */
    public function compile($factoryAccessor)
    {
        return $this->convertToPhpCode([$this->checkout], $factoryAccessor);
    }
}
