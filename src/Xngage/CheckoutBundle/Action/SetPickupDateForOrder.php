<?php

namespace Xngage\CheckoutBundle\Action;

use Doctrine\ORM\EntityManager;
use Oro\Bundle\CheckoutBundle\Entity\Checkout;
use Oro\Bundle\OrderBundle\Entity\Order;
use Oro\Component\Action\Action\AbstractAction;
use Oro\Component\Action\Exception\InvalidParameterException;
use Oro\Component\ConfigExpression\ContextAccessor;
use Oro\Component\ConfigExpression\Exception\InvalidArgumentException;

class SetPickupDateForOrder extends AbstractAction
{
    const NAME = 'set_pickup_date';
    const CHECKOUT = 'checkout';
    const ORDER = 'order';

    /**
     * @var Checkout
     */
    protected $checkout;

    /**
     * @var Order
     */
    protected $order;
    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function __construct(
        ContextAccessor $contextAccessor,
        EntityManager $entityManager
    ) {
        $this->entityManager = $entityManager;

        parent::__construct($contextAccessor);
    }

    /**
     * {@inheritDoc}
     */
    protected function executeAction($context)
    {
        /** @var Checkout $checkout */
        $checkout = $this->contextAccessor->getValue($context, $this->checkout);
        /** @var Order $order */
        $order = $this->contextAccessor->getValue($context, $this->order);
        if (!is_object($checkout)) {
            throw new InvalidParameterException(sprintf('Action "%s" expects reference to entity as parameter, %s is given.', static::NAME, gettype($checkout)));
        }
        if (!is_object($checkout)) {
            throw new InvalidParameterException(sprintf('Action "%s" expects reference to entity as parameter, %s is given.', static::NAME, gettype($checkout)));
        }

        $order->setPickupDay($checkout->getPickupDay());
        $order->setPickupTime($checkout->getPickupTime());

        $this->entityManager->persist($checkout);
    }

    /**
     * {@inheritDoc}
     */
    public function initialize(array $options)
    {
        if (!array_key_exists(self::CHECKOUT, $options)) {
            throw new InvalidArgumentException(sprintf('Missing "%s" option', self::CHECKOUT));
        }
        $this->checkout = $options[self::CHECKOUT];

        if (!array_key_exists(self::ORDER, $options)) {
            throw new InvalidArgumentException(sprintf('Missing "%s" option', self::ORDER));
        }
        $this->order = $options[self::ORDER];

        return $this;
    }
}
