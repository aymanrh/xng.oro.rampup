<?php

namespace Xngage\XERPBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Xngage\XERPBundle\DependencyInjection\XngageXERPExtension;

class XngageXERPBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        if (!$this->extension) {
            $this->extension = new XngageXERPExtension();
        }

        return $this->extension;
    }
}
