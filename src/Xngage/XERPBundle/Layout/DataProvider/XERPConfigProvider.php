<?php

namespace Xngage\XERPBundle\Layout\DataProvider;

use Doctrine\Persistence\ManagerRegistry;
use Oro\Bundle\MoneyOrderBundle\Method\Config\Factory\MoneyOrderConfigFactoryInterface;
use Oro\Bundle\MoneyOrderBundle\Method\Config\MoneyOrderConfigInterface;
use Psr\Log\LoggerInterface;
use Xngage\XERPBundle\Entity\XERPSettings;

class XERPConfigProvider
{
    /**
     * @var ManagerRegistry
     */
    protected $doctrine;

    /**
     * @var MoneyOrderConfigFactoryInterface
     */
    protected $configFactory;

    /**
     * @var MoneyOrderConfigInterface[]
     */
    protected $configs;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(
        ManagerRegistry $doctrine,
        LoggerInterface $logger
    ) {
        $this->doctrine = $doctrine;
        $this->logger = $logger;
    }


    /**
     * {@inheritDoc}
     */
    public function getERPConfigs()
    {
        return $this->getEnabledIntegrationSettings();
    }

    /**
     * {@inheritDoc}
     */
    public function getERPConfig($identifier)
    {
        $erpConfigs = $this->getERPConfigs();

        if ([] === $erpConfigs || false === array_key_exists($identifier, $erpConfigs)) {
            return null;
        }

        return $erpConfigs[$identifier];
    }

    /**
     * {@inheritDoc}
     */
    public function hasERPConfig($identifier)
    {
        return null !== $this->getERPConfig($identifier);
    }

    /**
     * @return XERPSettings[]
     */
    protected function getEnabledIntegrationSettings()
    {
        try {
            return $this->doctrine->getManagerForClass('XngageXERPBundle:XERPSettings')
                ->getRepository('XngageXERPBundle:XERPSettings')
                ->findWithEnabledChannel();
        } catch (\UnexpectedValueException $e) {
            $this->logger->critical($e->getMessage());

            return [];
        }
    }
}
