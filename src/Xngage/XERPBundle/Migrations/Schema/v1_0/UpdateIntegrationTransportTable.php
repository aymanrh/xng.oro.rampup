<?php

namespace Xngage\XERPBundle\Migrations\Schema\v1_0;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\SchemaException;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

class UpdateIntegrationTransportTable implements Migration
{
    /**
     * @param Schema   $schema
     * @param QueryBag $queries
     *
     * @throws SchemaException
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        $this->updateOroIntegrationTransportTable($schema);
    }

    /**
     * @param Schema $schema
     *
     * @throws SchemaException
     */
    private function updateOroIntegrationTransportTable(Schema $schema)
    {
        $table = $schema->getTable('oro_integration_transport');
        if (!$table->hasColumn('xngage_x_erp_url')) {
            $table->addColumn('xngage_x_erp_url', 'string', ['notnull' => false, 'length' => 255]);
        }
        if (!$table->hasColumn('xngage_x_erp_username')) {
            $table->addColumn('xngage_x_erp_username', 'string', ['notnull' => false, 'length' => 255]);
        }
        if (!$table->hasColumn('xngage_x_erp_password')) {
            $table->addColumn('xngage_x_erp_password', 'string', ['notnull' => false, 'length' => 255]);
        }
    }
}
