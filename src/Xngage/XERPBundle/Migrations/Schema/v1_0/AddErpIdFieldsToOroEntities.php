<?php

namespace Xngage\XERPBundle\Migrations\Schema\v1_0;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\ActivityBundle\Migration\Extension\ActivityExtension;
use Oro\Bundle\ActivityBundle\Migration\Extension\ActivityExtensionAwareInterface;
use Oro\Bundle\EntityExtendBundle\EntityConfig\ExtendScope;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

class AddErpIdFieldsToOroEntities implements Migration, ActivityExtensionAwareInterface
{
    /**
     * @internal
     */
    const PRODUCT_TABLE_NAME = 'oro_product';

    /** @var ActivityExtension */
    protected $activityExtension;

    /**
     * {@inheritDoc}
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        $table = $schema->getTable('oro_catalog_category');
        if (!$table->hasColumn('erp_id')) {
            $table->addColumn(
                'erp_id',
                'integer',
                ['oro_options' => [
                    'extend' => [
                        'is_extend' => true,
                        'owner' => ExtendScope::OWNER_CUSTOM,
                        'cascade' => ['persist', 'remove'],
                        'on_delete' => 'CASCADE',
                    ],
                    'entity' => ['label' => 'ERP ID'],
                    'datagrid' => ['is_visible' => false],
                ]]
            );
        }
    }

    /**
     * {@inheritDoc}
     */
    public function setActivityExtension(ActivityExtension $activityExtension)
    {
        $this->activityExtension = $activityExtension;
    }
}
