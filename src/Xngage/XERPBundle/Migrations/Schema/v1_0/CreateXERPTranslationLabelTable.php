<?php

namespace  Xngage\XERPBundle\Migrations\Schema\v1_0;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\SchemaException;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

class CreateXERPTranslationLabelTable implements Migration
{
    /**
     * @throws SchemaException
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        $this->createXngageXERPTransportLabelTable($schema);
        $this->addXngageXERPTransportLabelForeignKeys($schema);
    }

    private function createXngageXERPTransportLabelTable(Schema $schema)
    {
        if (!$schema->hasTable('xngage_x_erp_trans_label')) {
            $table = $schema->createTable('xngage_x_erp_trans_label');

            $table->addColumn('transport_id', 'integer', []);
            $table->addColumn('localized_value_id', 'integer', []);

            $table->setPrimaryKey(['transport_id', 'localized_value_id']);
            $table->addIndex(['transport_id'], 'xngage_x_erp_trans_label_transport_id', []);
            $table->addUniqueIndex(['localized_value_id'], 'xngage_x_erp_trans_label_localized_value_id', []);
        }
    }

    /**
     * @throws SchemaException
     */
    private function addXngageXERPTransportLabelForeignKeys(Schema $schema)
    {
        if (!$schema->hasTable('xngage_x_erp_trans_label')) {
            $table = $schema->getTable('xngage_x_erp_trans_label');

            $table->addForeignKeyConstraint(
                $schema->getTable('oro_fallback_localization_val'),
                ['localized_value_id'],
                ['id'],
                ['onDelete' => 'CASCADE', 'onUpdate' => null]
            );
            $table->addForeignKeyConstraint(
                $schema->getTable('oro_integration_transport'),
                ['transport_id'],
                ['id'],
                ['onDelete' => 'CASCADE', 'onUpdate' => null]
            );
        }
    }
}
