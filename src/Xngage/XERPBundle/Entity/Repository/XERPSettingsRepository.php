<?php

namespace Xngage\XERPBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Oro\Bundle\SecurityBundle\ORM\Walker\AclHelper;
use Xngage\XERPBundle\Entity\XERPSettings;

class XERPSettingsRepository extends EntityRepository
{
    /**
     * @var AclHelper
     */
    private $aclHelper;

    /**
     * @param AclHelper $aclHelper
     */
    public function setAclHelper(AclHelper $aclHelper)
    {
        $this->aclHelper = $aclHelper;
    }

    /**
     * @return XERPSettings[]
     */
    public function findWithEnabledChannel()
    {
        $qb = $this->createQueryBuilder('xerps');

        $qb
            ->join('xerps.channel', 'ch')
            ->where('ch.enabled = true')
            ->orderBy('xerps.id');

        return $this->aclHelper->apply($qb)->getResult();
    }
}
