<?php

namespace Xngage\XERPBundle\Form\Type;

use Oro\Bundle\FormBundle\Form\Type\OroEncodedPlaceholderPasswordType;
use Oro\Bundle\LocaleBundle\Form\Type\LocalizedFallbackValueCollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Xngage\XERPBundle\Entity\XERPSettings;

class XERPSettingsType extends AbstractType
{
    const BLOCK_PREFIX = 'xngage_x_erp_settings';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'url',
                TextType::class,
                [
                    'label' => 'xngage.x_erp.transport.url.label',
                    'required' => true,
                ]
            )
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'xngage.x_erp.transport.username.label',
                    'required' => true,
                ]
            )
            ->add(
                'password',
                OroEncodedPlaceholderPasswordType::class,
                [
                    'label' => 'xngage.x_erp.transport.password.label',
                    'required' => true,
                ]
            )
            ->add(
                'labels',
                LocalizedFallbackValueCollectionType::class,
                [
                    'label' => 'xngage.x_erp.transport.labels.label',
                    'required' => true,
                    'entry_options' => [
                        'constraints' => [new NotBlank()],
                    ],
                ]
            );
    }

    /**
     * {@inheritDoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => XERPSettings::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return self::BLOCK_PREFIX;
    }
}
