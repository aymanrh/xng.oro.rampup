<?php

namespace Xngage\XERPBundle\Integration;

use Oro\Bundle\IntegrationBundle\Provider\ChannelInterface;
use Oro\Bundle\IntegrationBundle\Provider\IconAwareIntegrationInterface;

class XERPChannelType implements ChannelInterface, IconAwareIntegrationInterface
{
    const TYPE = 'x_erp';

    /**
     * {@inheritdoc}
     */
    public function getLabel()
    {
        return 'xngage.x_erp.channel_type.label';
    }

    /**
     * {@inheritdoc}
     */
    public function getIcon()
    {
        return 'bundles/xngagexerp/img/x-erp.png';
    }
}
