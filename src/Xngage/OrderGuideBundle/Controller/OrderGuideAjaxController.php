<?php

namespace Xngage\OrderGuideBundle\Controller;

use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ProductBundle\Entity\Repository\ProductUnitRepository;
use Oro\Bundle\ShoppingListBundle\Entity\LineItem;
use Oro\Bundle\ShoppingListBundle\Entity\Repository\LineItemRepository;
use Oro\Bundle\ShoppingListBundle\Entity\ShoppingList;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Xngage\OrderGuideBundle\Entity\OrderGuide;
use Xngage\OrderGuideBundle\Entity\Repository\OrderGuideRepository;

/**
 * The AJAX controller for the website menu.
 *
 * @Route("/order-guide")
 */
class OrderGuideAjaxController extends AbstractController
{
    /**
     * @var OrderGuideRepository
     */
    protected $orderGuideRepository;

    /**
     * @var ProductUnitRepository
     */
    protected $productUnitRepository;

    /**
     * @var LineItemRepository
     */
    protected $lineItemRepository;

    public function __construct(
        OrderGuideRepository $orderGuideRepository,
        ProductUnitRepository $productUnitRepository,
        LineItemRepository $lineItemRepository
    ) {
        $this->orderGuideRepository = $orderGuideRepository;
        $this->productUnitRepository = $productUnitRepository;
        $this->lineItemRepository = $lineItemRepository;
    }

    /**
     * @Route(
     *     "/add-product/{productId}",
     *     name="xngage_order_guide_ajax_add_product",
     *     requirements={"productId"="\d+"},
     *     methods={"POST"}
     * )
     * @ParamConverter("product", class="OroProductBundle:Product", options={"id" = "productId"})
     *
     * @return JsonResponse
     */
    public function createAction(Request $request, Product $product)
    {
        $entityManager = $this->getDoctrine()->getManager();
        if ($product) {
            $unitCode = $request->get('unit');
            $unit = $this->productUnitRepository->findOneBy(['code' => $unitCode]);
            $qty = (float) $request->get('qty');

            $orderGuide = $this->orderGuideRepository->findOneBy(
                ['customerUser' => $this->getUser(), 'product' => $product, 'unit' => $unit]
            );

            if ($orderGuide) {
                $orderGuide->setQuantity($orderGuide->getQuantity() + $qty);
            } else {
                $orderGuide = new OrderGuide();
                $orderGuide->setCustomerUser($this->getUser())
                    ->setProduct($product)
                    ->setUnit($unit)
                    ->setQuantity($qty);
            }

            $entityManager->persist($orderGuide);
            $entityManager->flush();
        }

        return new JsonResponse(null, Response::HTTP_OK);
    }

    /**
     * @Route(
     *     "/add-all/{shoppingListId}",
     *      name="xngage_order_guide_ajax_add_all_to_shopping_list",
     *     requirements={"shoppingListId"="\d+"},
     *     methods={"POST"}
     * )
     * @ParamConverter("shoppingList", class="OroShoppingListBundle:ShoppingList", options={"id" = "shoppingListId"})
     *
     * @return JsonResponse
     */
    public function addAllToShoppingListAction(Request $request, ShoppingList $shoppingList)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $orderGuides = $this->orderGuideRepository->findBy(['customerUser' => $this->getUser()]);

        foreach ($orderGuides as $guide) {
            $lineItem = $this->lineItemRepository->findOneBy([
                'shoppingList' => $shoppingList,
                'product' => $guide->getProduct(),
                'unit' => $guide->getUnit(),
            ]);

            if (is_null($lineItem)) {
                $lineItem = new LineItem();
                $lineItem
                    ->setCustomerUser($shoppingList->getCustomerUser())
                    ->setOrganization($shoppingList->getOrganization())
                    ->setProduct($guide->getProduct())
                    ->setUnit($guide->getUnit())
                    ->setQuantity($guide->getQuantity());

                $shoppingList->addLineItem($lineItem);
                $entityManager->persist($shoppingList);
            } else {
                $lineItem->setQuantity($guide->getQuantity());
            }

            $entityManager->persist($lineItem);
        }

        $entityManager->flush();
        return new JsonResponse(null, Response::HTTP_OK);
    }
}
