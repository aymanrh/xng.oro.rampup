<?php

namespace Xngage\OrderGuideBundle\Controller\Frontend;

use Oro\Bundle\CustomerBundle\Entity\CustomerUser;
use Oro\Bundle\LayoutBundle\Annotation\Layout;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Xngage\OrderGuideBundle\Entity\Repository\OrderGuideRepository;

/**
 * Controller that manages order guide.
 */
class OrderGuideController extends AbstractController
{
    protected $orderGuideRepository;

    public function __construct(OrderGuideRepository $orderGuideRepository)
    {
        $this->orderGuideRepository = $orderGuideRepository;
    }

    /**
     * @Route("/", name="xngage_order_guide_frontend_index")
     * @Layout
     * @AclAncestor("xngage_order_guide_frontend_index")
     */
    public function indexAction()
    {
        if (!$this->getUser() instanceof CustomerUser) {
            throw $this->createAccessDeniedException();
        }

        /* @var OrderGuideRepository $orderGuideRepository */
        //$oderGuide = $this->orderGuideRepository->findOneByCustomerUser($this->getUser());

        return [
            'data' => [
                'customer' => $this->getUser(),
            ],
        ];
    }
}
