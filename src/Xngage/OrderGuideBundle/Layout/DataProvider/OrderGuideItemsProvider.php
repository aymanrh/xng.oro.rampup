<?php

namespace Xngage\OrderGuideBundle\Layout\DataProvider;

use Oro\Bundle\CustomerBundle\Entity\CustomerUser;
use Xngage\OrderGuideBundle\Entity\Repository\OrderGuideRepository;

class OrderGuideItemsProvider
{
    /**
     * @var OrderGuideRepository
     */
    protected $orderGuideRepository;

    public function __construct(OrderGuideRepository $orderGuideRepository)
    {
        $this->orderGuideRepository = $orderGuideRepository;
    }

    public function getCustomerOrderGuideItems(CustomerUser $customer)
    {
        $items = $this->orderGuideRepository->findBy(['customerUser' => $customer]);
        return $items;
    }
}
