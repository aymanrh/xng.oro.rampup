define(function(require) {
    'use strict';

    const routing = require('routing');
    const mediator = require('oroui/js/mediator');
    const messenger = require('oroui/js/messenger');
    const BaseComponent = require('oroui/js/app/components/base/component');
    const ShoppingListCollectionService = require('oroshoppinglist/js/shoppinglist-collection-service');
    const __ = require('orotranslation/js/translator');
    const _ = require('underscore');
    const $ = require('jquery');

    const AddAllToShoppingListComponent = BaseComponent.extend({

        shoppingListCollection: null,

        /**
         * @inheritDoc
         */
        constructor: function AddAllToShoppingListComponent(options) {
            AddAllToShoppingListComponent.__super__.constructor.call(this, options);
        },

        initialize: function(options) {
            this.options = _.defaults(options || {}, this.options);
            this.options._sourceElement.on('click', this.options._sourceElement, _.bind(this._onClick, this));
            ShoppingListCollectionService.shoppingListCollection.done((function(collection) {
                this.shoppingListCollection = collection;
            }).bind(this));
        },

        _onClick: function (event) {
            const $button = $(event.currentTarget);
            if ($button.data('disabled')) {
                return false;
            }

            const url = $button.data('url');
            const currentShoppingList = this._getCurrentShoppingList();
            if (!currentShoppingList) {
                return false;
            }

            const urlOptions = {shoppingListId: currentShoppingList.get('id')};
            this._addAllToShoppingList(url, urlOptions);
        },

        _addAllToShoppingList: function (url, urlOptions) {
            const self = this;

            mediator.execute('showLoading');
            $.ajax({
                type: 'POST',
                url: routing.generate(url, urlOptions),
                success: function(response) {
                    const currentShoppingList = self._getCurrentShoppingList();
                    mediator.trigger('shopping-list:refresh');
                    messenger.notificationFlashMessage('success', __(`All items have been added to the <a href="/customer/shoppinglist/${currentShoppingList.get('id')}">${currentShoppingList.get('label')}</a> successfully`));
                },
                complete: function() {
                    mediator.execute('hideLoading');
                }
            });
        },

        _getCurrentShoppingList: function() {
            return this.shoppingListCollection.find({is_current: true});
        },
    });

    return AddAllToShoppingListComponent;
});