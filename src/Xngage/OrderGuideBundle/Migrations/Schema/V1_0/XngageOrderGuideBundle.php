<?php

namespace Xngage\OrderGuideBundle\Migrations\Schema\V1_0;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class XngageOrderGuideBundle implements Migration
{
    /**
     * {@inheritdoc}
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        /* Tables generation **/
        $this->createXngageOrderGuideTable($schema);

        /* Foreign keys generation **/
        $this->addXngageOrderGuideForeignKeys($schema);
    }

    /**
     * Create xngage_order_guide table.
     */
    protected function createXngageOrderGuideTable(Schema $schema)
    {
        if (!$schema->hasTable('xngage_order_guide')) {
            $table = $schema->createTable('xngage_order_guide');
            $table->addColumn('id', 'integer', ['autoincrement' => true]);
            $table->addColumn('product_id', 'integer', []);
            $table->addColumn('unit_code', 'string', ['length' => 255]);
            $table->addColumn('user_owner_id', 'integer', ['notnull' => false]);
            $table->addColumn('organization_id', 'integer', ['notnull' => false]);
            $table->addColumn('customer_user_id', 'integer', ['notnull' => false]);
            $table->addColumn('customer_id', 'integer', ['notnull' => false]);
            $table->addColumn('quantity', 'float', []);
            $table->addColumn('created_at', 'datetime', []);
            $table->addColumn('updated_at', 'datetime', []);
            $table->addIndex(['customer_id'], 'idx_223a3a959395c3f3', []);
            $table->addIndex(['updated_at'], 'idx_order_guide_updated_at', []);
            $table->addUniqueIndex(['product_id', 'customer_id', 'customer_user_id', 'unit_code'], 'xngage_order_guide_uidx');
            $table->addIndex(['customer_user_id'], 'idx_223a3a95bbb3772b', []);
            $table->setPrimaryKey(['id']);
            $table->addIndex(['created_at'], 'idx_order_guide_created_at', []);
            $table->addIndex(['user_owner_id'], 'idx_223a3a959eb185f9', []);
            $table->addIndex(['product_id'], 'idx_223a3a954584665a', []);
            $table->addIndex(['organization_id'], 'idx_223a3a9532c8a3de', []);
            $table->addIndex(['unit_code'], 'idx_223a3a95fbd3d1c2', []);
        }
    }

    /**
     * Add xngage_order_guide foreign keys.
     */
    protected function addXngageOrderGuideForeignKeys(Schema $schema)
    {
        if (!$schema->hasTable('xngage_order_guide')) {
            $table = $schema->getTable('xngage_order_guide');
            $table->addForeignKeyConstraint(
                $schema->getTable('oro_organization'),
                ['organization_id'],
                ['id'],
                ['onUpdate' => null, 'onDelete' => 'SET NULL']
            );
            $table->addForeignKeyConstraint(
                $schema->getTable('oro_product'),
                ['product_id'],
                ['id'],
                ['onUpdate' => null, 'onDelete' => 'CASCADE']
            );
            $table->addForeignKeyConstraint(
                $schema->getTable('oro_customer'),
                ['customer_id'],
                ['id'],
                ['onUpdate' => null, 'onDelete' => 'SET NULL']
            );
            $table->addForeignKeyConstraint(
                $schema->getTable('oro_user'),
                ['user_owner_id'],
                ['id'],
                ['onUpdate' => null, 'onDelete' => 'SET NULL']
            );
            $table->addForeignKeyConstraint(
                $schema->getTable('oro_customer_user'),
                ['customer_user_id'],
                ['id'],
                ['onUpdate' => null, 'onDelete' => 'CASCADE']
            );
            $table->addForeignKeyConstraint(
                $schema->getTable('oro_product_unit'),
                ['unit_code'],
                ['code'],
                ['onUpdate' => null, 'onDelete' => 'CASCADE']
            );
        }
    }
}
