<?php

namespace Xngage\OrderGuideBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Oro\Bundle\CustomerBundle\Entity\CustomerUser;
use Xngage\OrderGuideBundle\Entity\OrderGuide;

/**
 * @method OrderGuide|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderGuide|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderGuide[]    findAll()
 * @method OrderGuide[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderGuideRepository extends EntityRepository
{
    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByCustomerUser(CustomerUser $customerUser)
    {
        return $this->createQueryBuilder('guide')
            ->andWhere('guide.customerUser = :customerUser')
            ->setParameter('customerUser', $customerUser)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
