<?php

namespace Xngage\WarehouseBundle\Inventory;

use Oro\Bundle\EntityBundle\Fallback\EntityFallbackResolver;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\InventoryBundle\Entity\InventoryLevel;
use Oro\Bundle\InventoryBundle\Exception\InventoryLevelNotFoundException;
use Oro\Bundle\OrderBundle\Entity\OrderLineItem;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ProductBundle\Entity\ProductUnit;
use Oro\Bundle\WarehouseBundle\Entity\Repository\InventoryLevelRepository;
use Oro\Bundle\WarehouseBundle\Inventory\InventoryQuantityManager;
use Oro\Bundle\WarehouseBundle\Inventory\InventoryStatusHandler;
use Oro\Bundle\WarehouseBundle\Provider\EnabledWarehousesProvider;

class WarehouseManager
{
    /**
     * @var DoctrineHelper
     */
    protected $doctrineHelper;

    /**
     * @var InventoryQuantityManager
     */
    protected $quantityManager;

    /**
     * @var InventoryStatusHandler
     */
    protected $inventoryStatusHandler;

    /**
     * @var EnabledWarehousesProvider
     */
    protected $enabledWarehousesProvider;

    /**
     * @var EntityFallbackResolver
     */
    protected $entityFallbackResolver;

    public function __construct(
        DoctrineHelper $doctrineHelper,
        InventoryQuantityManager $quantityManager,
        InventoryStatusHandler $inventoryStatusHandler,
        EnabledWarehousesProvider $enabledWarehousesProvider,
        EntityFallbackResolver $entityFallbackResolver
    ) {
        $this->doctrineHelper = $doctrineHelper;
        $this->quantityManager = $quantityManager;
        $this->inventoryStatusHandler = $inventoryStatusHandler;
        $this->enabledWarehousesProvider = $enabledWarehousesProvider;
        $this->entityFallbackResolver = $entityFallbackResolver;
    }

    /**
     * @return bool
     *
     * @throws InventoryLevelNotFoundException
     */
    public function processDecrementForOrderLineItem(OrderLineItem $orderLineItem)
    {
        $sortedWarehouseIds = $this->enabledWarehousesProvider->getEnabledSortedWarehouseIds();
        $warehousesAvailableQuantityArray = [];
        $totalAvailableQuantityInAllWarehouses = 0;
        foreach ($sortedWarehouseIds as $warehouseId) {
            $inventoryLevel = $this->getInventoryLevel(
                $orderLineItem->getProduct(),
                $orderLineItem->getProductUnit(),
                $warehouseId
            );

            if (!$inventoryLevel) {
                throw new InventoryLevelNotFoundException();
            }

            if ($this->quantityManager->canDecrementInventory($inventoryLevel, $orderLineItem->getQuantity())) {
                $this->quantityManager->decrementInventory($inventoryLevel, $orderLineItem->getQuantity());
                $this->inventoryStatusHandler->updateInventoryStatus(
                    $sortedWarehouseIds,
                    $orderLineItem->getProduct(),
                    $orderLineItem->getProductUnit()
                );
                $orderLineItem->setWarehouse($inventoryLevel->getWarehouse());

                return true;
            }

            $product = $inventoryLevel->getProduct();
            $inventoryThreshold = $this->entityFallbackResolver->getFallbackValue($product, 'inventoryThreshold');
            $decrementQuantity = $this->entityFallbackResolver->getFallbackValue($product, 'decrementQuantity');
            if ($decrementQuantity) {
                $warehousesAvailableQuantityArray[$warehouseId] = $inventoryLevel->getQuantity() - $inventoryThreshold;
                $totalAvailableQuantityInAllWarehouses += $inventoryLevel->getQuantity() - $inventoryThreshold;
            }
        }

        if ($totalAvailableQuantityInAllWarehouses >= $orderLineItem->getQuantity()) {
            $quantityToDecrement = $orderLineItem->getQuantity();
            foreach ($sortedWarehouseIds as $warehouseId) {
                $inventoryLevel = $this->getInventoryLevel(
                    $orderLineItem->getProduct(),
                    $orderLineItem->getProductUnit(),
                    $warehouseId
                );

                if ($quantityToDecrement >= $warehousesAvailableQuantityArray[$warehouseId]) {
                    $this->quantityManager->decrementInventory($inventoryLevel, $warehousesAvailableQuantityArray[$warehouseId]);
                    $quantityToDecrement -= $warehousesAvailableQuantityArray[$warehouseId];
                } else {
                    $this->quantityManager->decrementInventory($inventoryLevel, $quantityToDecrement);
                    $quantityToDecrement -= $quantityToDecrement;
                }

                if (0 == $quantityToDecrement) {
                    $this->inventoryStatusHandler->updateInventoryStatus(
                        $sortedWarehouseIds,
                        $orderLineItem->getProduct(),
                        $orderLineItem->getProductUnit()
                    );
                    $orderLineItem->setWarehouse($inventoryLevel->getWarehouse());

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $warehouseId
     *
     * @return InventoryLevel|null
     */
    protected function getInventoryLevel(Product $product, ProductUnit $productUnit, $warehouseId)
    {
        /** @var InventoryLevelRepository $repository */
        $repository = $this->doctrineHelper->getEntityRepository(InventoryLevel::class);

        return $repository->getLevelByProductProductUnitAndWarehouse($product, $productUnit, $warehouseId);
    }
}
