<?php

namespace Xngage\WarehouseBundle\EventListener;

use Oro\Bundle\CheckoutBundle\DataProvider\Manager\CheckoutLineItemsManager;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\InventoryBundle\Inventory\InventoryStatusHandler;
use Oro\Bundle\OrderBundle\Entity\OrderLineItem;
use Oro\Bundle\WarehouseBundle\EventListener\CreateOrderEventListener as BaseListener;
use Oro\Bundle\WarehouseBundle\Inventory\InventoryQuantityManager;
use Oro\Bundle\WarehouseBundle\Inventory\WarehouseManager;
use Oro\Component\Action\Event\ExtendableActionEvent;
use Xngage\WarehouseBundle\Inventory\WarehouseManager as XngageWarehouseManager;

/**
 * Check that there are enough products at warehouse during checkout process.
 */
class CreateOrderEventListener extends BaseListener
{
    /**
     * @var XngageWarehouseManager
     */
    protected $xngageWarehouseManager;

    /**
     * @var WarehouseManager
     */
    protected $warehouseManager;

    /**
     * @var InventoryQuantityManager
     */
    protected $quantityManager;

    public function __construct(
        XngageWarehouseManager $xngageWarehouseManager,
        WarehouseManager $warehouseManager,
        InventoryQuantityManager $inventoryQuantityManager,
        InventoryStatusHandler $inventoryStatusHandler,
        DoctrineHelper $doctrineHelper,
        CheckoutLineItemsManager $checkoutLineItemsManager
    ) {
        parent::__construct(
            $warehouseManager,
            $inventoryQuantityManager,
            $inventoryStatusHandler,
            $doctrineHelper,
            $checkoutLineItemsManager
        );
        $this->xngageWarehouseManager = $xngageWarehouseManager;
    }

    public function onCreateOrder(ExtendableActionEvent $event)
    {
        if (!$this->isCorrectOrderContext($event->getContext())) {
            return;
        }

        $orderLineItems = $this->checkoutLineItemsManager->getData($event->getContext()->getEntity());
        /** @var OrderLineItem $lineItem */
        foreach ($orderLineItems as $lineItem) {
            if (!$this->quantityManager->shouldDecrement($lineItem->getProduct())) {
                continue;
            }

            $inventoryDecremented = $this->xngageWarehouseManager->processDecrementForOrderLineItem($lineItem);
            if (!$inventoryDecremented) {
                $mainInventoryLevel = $this->warehouseManager->getMainWarehouseInventoryLevel(
                    $lineItem->getProduct(),
                    $lineItem->getProductUnit()
                );

                if ($mainInventoryLevel) {
                    $this->quantityManager->handleRemainingQuantities($mainInventoryLevel, $lineItem->getQuantity());
                    $this->statusHandler->changeInventoryStatusWhenDecrement($mainInventoryLevel);
                }
            }
        }
    }
}
