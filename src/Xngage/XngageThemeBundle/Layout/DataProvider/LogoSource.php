<?php

namespace Xngage\XngageThemeBundle\Layout\DataProvider;

use Oro\Bundle\AttachmentBundle\Entity\File;
use Oro\Bundle\AttachmentBundle\Manager\AttachmentManager;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Xngage\XngageThemeBundle\DependencyInjection\Configuration;

class LogoSource
{
    /** @var ConfigManager */
    private $configManager;

    /** @var DoctrineHelper */
    private $doctrineHelper;

    /** @var AttachmentManager */
    private $attachmentManager;

    public function __construct(
        ConfigManager $configManager,
        DoctrineHelper $doctrineHelper,
        AttachmentManager $attachmentManager
    ) {
        $this->configManager = $configManager;
        $this->doctrineHelper = $doctrineHelper;
        $this->attachmentManager = $attachmentManager;
    }

    public function getLogoPath()
    {
        $filePath = '';
        $websiteLogoKey = Configuration::ROOT_NODE.'.'.Configuration::WEBSITE_LOGO_IMAGE;
        $imageId = $this->configManager->get($websiteLogoKey);

        if ($imageId && $image = $this->doctrineHelper->getEntityRepositoryForClass(File::class)->find($imageId)) {
            /** @var File $image */
            $filePath = $this->attachmentManager->getFileUrl($image);
        }

        return $filePath;
    }

    public function getLogoWidth()
    {
        $websiteLogoKey = Configuration::ROOT_NODE.'.'.Configuration::WEBSITE_LOGO_WIDTH;

        return $this->configManager->get($websiteLogoKey);
    }

    public function getLogoHeight()
    {
        $websiteLogoKey = Configuration::ROOT_NODE.'.'.Configuration::WEBSITE_LOGO_HEIGHT;

        return $this->configManager->get($websiteLogoKey);
    }
}
