<?php

namespace Xngage\XngageThemeBundle\Layout\DataProvider;

use Oro\Bundle\ConfigBundle\Config\ConfigManager;

class XngageThemeSettings
{
    /**
     * @var ConfigManager
     */
    protected $configManager;

    public function __construct(ConfigManager $configManager)
    {
        $this->configManager = $configManager;
    }

    /**
     * @return bool
     */
    public function isBrandEnabled()
    {
        return (bool) $this->configManager->get('xngage_xngage_theme.display_brand');
    }
}
