<?php


namespace Xngage\XngageThemeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Xngage\XngageThemeBundle\DependencyInjection\XngageXngageThemeExtension;

class XngageXngageThemeBundle extends Bundle
{
    public function getContainerExtension()
    {
        if (!$this->extension) {
            $this->extension = new XngageXngageThemeExtension();
        }

        return $this->extension;
    }
}
