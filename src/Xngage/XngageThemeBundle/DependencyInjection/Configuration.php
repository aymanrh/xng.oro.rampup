<?php

namespace Xngage\XngageThemeBundle\DependencyInjection;

use Oro\Bundle\ConfigBundle\DependencyInjection\SettingsBuilder;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    const ROOT_NODE = XngageXngageThemeExtension::ALIAS;
    const DISPLAY_BRAND = 'display_brand';
    const WEBSITE_LOGO_IMAGE = 'website_logo_image';
    const WEBSITE_LOGO_WIDTH = 'website_logo_width';
    const WEBSITE_LOGO_HEIGHT = 'website_logo_height';

    /**
     * {@inheritDoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(self::ROOT_NODE);

        $rootNode = $treeBuilder->getRootNode();

        SettingsBuilder::append(
            $rootNode,
            [
                self::DISPLAY_BRAND => ['type' => 'boolean', 'value' => false],
                self::WEBSITE_LOGO_IMAGE => ['value' => null],
                self::WEBSITE_LOGO_WIDTH => ['value' => null],
                self::WEBSITE_LOGO_HEIGHT => ['value' => null],
            ]
        );

        return $treeBuilder;
    }
}
