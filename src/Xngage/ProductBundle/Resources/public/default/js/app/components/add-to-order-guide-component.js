define(function(require) {
    'use strict';

    const routing = require('routing');
    const mediator = require('oroui/js/mediator');
    const messenger = require('oroui/js/messenger');
    const BaseComponent = require('oroui/js/app/components/base/component');
    const __ = require('orotranslation/js/translator');
    const _ = require('underscore');
    const $ = require('jquery');

    const AddToOrderGuideComponent = BaseComponent.extend({

        /**
         * @inheritDoc
         */
        constructor: function AddToOrderGuideComponent(options) {
            AddToOrderGuideComponent.__super__.constructor.call(this, options);
        },

        initialize: function(options) {
            this.options = _.defaults(options || {}, this.options);
            this.options._sourceElement.on('click', this.options._sourceElement, _.bind(this._onClick, this));
        },

        _onClick: function (event) {
            const $button = $(event.currentTarget);
            if ($button.data('disabled')) {
                return false;
            }
            const $form = $button.closest('form');
            const qty = $form.find('[data-name="field__quantity"]:enabled').val();
            const unit = $form.find('[data-name="field__unit"]:enabled').val();
            const url = $button.data('url');
            const formData = {
                qty: qty,
                unit: unit
            };
            const urlOptions = {productId: this.options.productModel.id};

            this._addProductToOrderGuide(url, urlOptions, formData);
        },

        _addProductToOrderGuide: function (url, urlOptions, formData) {
            const self = this;

            mediator.execute('showLoading');
            $.ajax({
                type: 'POST',
                url: url, //routing.generate(url, urlOptions),
                data: formData,
                success: function(response) {
                    messenger.notificationFlashMessage('success', __('Product has been added to <a href="customer/order-guide"> your order guide</a> successfully!'));
                },
                complete: function() {
                    mediator.execute('hideLoading');
                }
            });
        }
    });

    return AddToOrderGuideComponent;
});