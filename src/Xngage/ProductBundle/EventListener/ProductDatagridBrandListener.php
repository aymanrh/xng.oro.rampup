<?php

namespace Xngage\ProductBundle\EventListener;

use Doctrine\ORM\EntityRepository;
use Oro\Bundle\DataGridBundle\Datasource\ResultRecord;
use Oro\Bundle\DataGridBundle\Event\PreBuild;
use Oro\Bundle\DataGridBundle\Extension\Formatter\Property\PropertyInterface;
use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\ProductBundle\Entity\Repository\BrandRepository;
use Oro\Bundle\RedirectBundle\Entity\Repository\SlugRepository;
use Oro\Bundle\SearchBundle\Datagrid\Event\SearchResultAfter;
use Oro\Bundle\SearchBundle\Datagrid\Event\SearchResultBefore;
use Oro\Bundle\SearchBundle\Query\Criteria\Criteria;
use Symfony\Component\HttpFoundation\RequestStack;

class ProductDatagridBrandListener
{
    const COLUMN_BRAND = 'brand';

    /**
     * @var DoctrineHelper
     */
    private $doctrineHelper;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(DoctrineHelper $doctrineHelper, RequestStack $requestStack)
    {
        $this->doctrineHelper = $doctrineHelper;
        $this->requestStack = $requestStack;
    }

    /**
     * @param PreBuild $event
     */
    public function onPreBuild(PreBuild $event)
    {
        $config = $event->getConfig();

        $config->offsetAddToArrayByPath(
            '[properties]',
            [
                self::COLUMN_BRAND => [
                    'type' => 'field',
                    'frontend_type' => PropertyInterface::TYPE_STRING,
                ],
            ]
        );
    }

    /**
     * @param SearchResultBefore $event
     */
    public function onBeforeResult(SearchResultBefore $event)
    {
        $request = $this->requestStack->getCurrentRequest();

        if ('xngage_product_frontend_by_brand' === $request->attributes->get('_route')) {
            $slug = $this->getSlugRepository()->findOneBy(['slugPrototype' => $request->attributes->get('slug')]);
            if ($slug) {
                $brand = $this->getBrandRepository()->findOneBySlug($slug);
                if ($brand) {
                    $query = $event->getQuery();
                    $query->addWhere(Criteria::expr()->eq('integer.brand_id', $brand->getId()));
                }
            }
        }
    }

    /**
     * @param SearchResultAfter $event
     */
    public function onResultAfter(SearchResultAfter $event)
    {
        /** @var ResultRecord[] $records */
        $records = $event->getRecords();
        $entityManager = $this->doctrineHelper->getEntityManagerForClass(Product::class);

        foreach ($records as $record) {
            /** @var Product $product */
            if ($product = $entityManager->getReference(Product::class, $record->getValue('id'))) {
                if ($product->getBrand()) {
                    $record->addData([self::COLUMN_BRAND => $product->getBrand()->getDefaultName()]);
                }
            }
        }
    }

    /**
     * @return BrandRepository|EntityRepository
     */
    protected function getBrandRepository()
    {
        return $this->doctrineHelper->getEntityRepository('OroProductBundle:Brand');
    }

    /**
     * @return SlugRepository|EntityRepository
     */
    protected function getSlugRepository()
    {
        return $this->doctrineHelper->getEntityRepository('OroRedirectBundle:Slug');
    }
}
