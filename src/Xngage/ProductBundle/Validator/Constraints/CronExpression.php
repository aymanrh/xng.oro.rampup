<?php

namespace Xngage\ProductBundle\Validator\Constraints;

use Oro\Bundle\ValidationBundle\Validator\Constraints\AliasAwareConstraintInterface;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Class CronExpression
 * TODO:: to be moved to XngageCoreBundle.
 */
class CronExpression extends Regex implements AliasAwareConstraintInterface
{
    const ALIAS = 'cron_expression';

    /** @var string */
    public $message = 'This value should be a valid cron expression latin letters.';

    /** @var string */
    public $pattern = '/^(@(annually|yearly|monthly|weekly|daily|hourly|reboot))|(@every (\d+(ns|us|µs|ms|s|m|h))+)|((((\d+,)+\d+|(\d+(\/|-)\d+)|\d+|\*) ?){5,7})$/';

    /**
     * {@inheritdoc}
     */
    public function getDefaultOption()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function validatedBy()
    {
        return 'Symfony\Component\Validator\Constraints\RegexValidator';
    }

    /**
     * {@inheritdoc}
     */
    public function getAlias()
    {
        return self::ALIAS;
    }
}
