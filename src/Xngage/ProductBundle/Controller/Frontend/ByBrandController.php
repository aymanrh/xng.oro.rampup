<?php

namespace Xngage\ProductBundle\Controller\Frontend;

use Oro\Bundle\LayoutBundle\Annotation\Layout;
use Oro\Bundle\ProductBundle\DataGrid\DataGridThemeHelper;
use Oro\Bundle\ProductBundle\Entity\Product;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ByBrandController extends AbstractController
{
    /**
     * @var DataGridThemeHelper
     */
    protected $dataGridThemeHelper;

    /**
     * ByBrandController constructor.
     * @param DataGridThemeHelper $dataGridThemeHelper
     */
    public function __construct(DataGridThemeHelper $dataGridThemeHelper)
    {
        $this->dataGridThemeHelper = $dataGridThemeHelper;
    }

    /**
     * View list of products.
     *
     * @Route("/products/by-brand/{slug}", name="xngage_product_frontend_by_brand")
     * @Layout(vars={"entity_class", "grid_config", "theme_name"})
     * @AclAncestor("oro_product_frontend_view")
     *
     * @return array
     */
    public function indexAction()
    {
        return [
            'entity_class' => Product::class,
            'theme_name' => $this->dataGridThemeHelper->getTheme('frontend-product-search-grid'),
            'grid_config' => [
                'frontend-product-search-grid',
            ],
        ];
    }
}
