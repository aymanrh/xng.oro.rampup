<?php

namespace Xngage\ProductBundle\Api;

use Http\Client\Common\HttpMethodsClientInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CoreOroApi
{
    const ORO_BASE_URL = 'http://orocommerce.local';
    const BACKEND_PREFIX = 'admin';
    const CLIENT_ID = '9EFFZUao-3qaDmDi7B_-qFTXS_GPSR7w';
    const CLIENT_SECRET = 'FmnzmrAQBjMzhTW64ALDAtpJJXU-ZKTcNACCPV7ST-zVdMPOTL0ZIlB6hp9OgXroPJJei-gAsH-VESNQwoi0jA';
    const ADMIN_USERNAME = 'othman';
    const ADMIN_PASSWORD = 'Pass@123';

    const AUTHENTICATION_ENDPOINT = 'oauth2-token';

    /**
     * @var HttpMethodsClientInterface
     */
    protected $curlClient;

    /**
     * @var string
     */
    private $accessToken;

    public function __construct(HttpMethodsClientInterface $curlClient)
    {
        $this->curlClient = $curlClient;
    }

    /**
     * @return string|null
     */
    public function getAccessToken()
    {
        if (null === $this->accessToken) {
            $this->accessToken = $this->fetchOAuthToken();
        }

        return $this->accessToken;
    }

    /**
     * @throws \Http\Client\Exception
     */
    public function refreshAccessToken()
    {
        $this->accessToken = $this->fetchOAuthToken();
    }

    /**
     * @return string|null
     *
     * @throws \Http\Client\Exception
     */
    protected function fetchOAuthToken()
    {
        $params = [
            'grant_type' => 'password',
            'client_id' => self::CLIENT_ID,
            'client_secret' => self::CLIENT_SECRET,
            'username' => self::ADMIN_USERNAME,
            'password' => self::ADMIN_PASSWORD,
        ];
        $content = http_build_query($params);

        $response = $this->curlClient->post(self::ORO_BASE_URL.'/'.self::AUTHENTICATION_ENDPOINT, [], $content);
        if (Response::HTTP_OK === $response->getStatusCode()) {
            $data = json_decode((string) $response->getBody(), true);
            $accessToken = $data['access_token'];
            $refreshToken = $data['refresh_token'];
            $tokenType = $data['token_type'];
            $expiresIn = $data['expires_in'];

            return $accessToken;
        }

        return null;
    }

    /**
     * @return ResponseInterface|null
     *
     * @throws \Http\Client\Exception
     */
    public function callApi(string $type, string $endPoint, array $headers = [], array $body = [])
    {
        $headers = array_merge([
            'Authorization' => "Bearer {$this->getAccessToken()}",
            'Content-Type' => 'application/vnd.api+json',
            'Accept' => 'application/vnd.api+json',
        ], $headers);

        $uri = self::ORO_BASE_URL.'/'.self::BACKEND_PREFIX.'/api/'.$endPoint;
        $content = json_encode($body);
        $response = null;
        switch ($type) {
            case Request::METHOD_GET:
                $response = $this->curlClient->get($uri, $headers);
                break;
            case Request::METHOD_POST:
                $response = $this->curlClient->post($uri, $headers, $content);
                break;
        }

        if (Response::HTTP_UNAUTHORIZED === $response->getStatusCode()) {
            $this->refreshAccessToken();
            return $this->callApi($type, $endPoint, $headers, $body);
        }

        return $response;
    }
}
