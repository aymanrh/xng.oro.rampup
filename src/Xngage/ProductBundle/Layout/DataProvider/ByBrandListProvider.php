<?php

namespace Xngage\ProductBundle\Layout\DataProvider;

use Oro\Bundle\EntityBundle\ORM\DoctrineHelper;
use Oro\Bundle\ProductBundle\Entity\Brand;

class ByBrandListProvider
{
    /** @var DoctrineHelper */
    protected $doctrineHelper;

    /**
     * ByBrandListProvider constructor.
     */
    public function __construct(DoctrineHelper $doctrineHelper)
    {
        $this->doctrineHelper = $doctrineHelper;
    }

    /**
     * @return Brand[]|null
     */
    public function getActiveBrands()
    {
        return $this->doctrineHelper
            ->getEntityRepositoryForClass('OroProductBundle:Brand')
            ->findBy(['status' => Brand::STATUS_ENABLED]);
    }
}
