<?php
declare(strict_types=1);

namespace Xngage\ProductBundle\DependencyInjection;

use Oro\Bundle\ConfigBundle\DependencyInjection\SettingsBuilder;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    const ROOT_NODE = XngageProductExtension::ALIAS;
    const PRODUCTS_SYNC_CRON_SCHEDULE = 'products_sync_cron_schedule';
    const DEFAULT_CRON_SCHEDULE = '0 * * * *';

    /**
     * {@inheritDoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(self::ROOT_NODE);

        $rootNode = $treeBuilder->getRootNode();

        SettingsBuilder::append(
            $rootNode,
            [
                static::PRODUCTS_SYNC_CRON_SCHEDULE => ['value' => static::DEFAULT_CRON_SCHEDULE],
            ]
        );

        return $treeBuilder;
    }
}
