<?php

namespace Xngage\ProductBundle\Migrations\Schema\v1_0;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\ActivityBundle\Migration\Extension\ActivityExtension;
use Oro\Bundle\ActivityBundle\Migration\Extension\ActivityExtensionAwareInterface;
use Oro\Bundle\EntityExtendBundle\EntityConfig\ExtendScope;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;

class OroProductBundle implements Migration, ActivityExtensionAwareInterface
{
    /**
     * @internal
     */
    const PRODUCT_TABLE_NAME = 'oro_product';

    /** @var ActivityExtension */
    protected $activityExtension;

    /**
     * {@inheritDoc}
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        $table = $schema->getTable(self::PRODUCT_TABLE_NAME);
        if (!$table->hasColumn('volume')) {
            $table->addColumn(
                'volume',
                'string',
                ['oro_options' => [
                    'extend' => [
                        'is_extend' => true,
                        'owner' => ExtendScope::OWNER_CUSTOM,
                        'cascade' => ['persist', 'remove'],
                        'on_delete' => 'CASCADE',
                    ],
                    'entity' => ['label' => 'Volume'],
                    'datagrid' => ['is_visible' => false],
                ]]
            );
        }

        if (!$table->hasColumn('volumeUom')) {
            $table->addColumn(
                'volumeUom',
                'string',
                ['oro_options' => [
                    'extend' => [
                        'is_extend' => true,
                        'owner' => ExtendScope::OWNER_CUSTOM,
                        'cascade' => ['persist', 'remove'],
                        'on_delete' => 'CASCADE',
                    ],
                    'entity' => ['label' => 'Volume unit of measurement'],
                    'datagrid' => ['is_visible' => false],
                ]]
            );
        }

        if (!$table->hasColumn('weight')) {
            $table->addColumn(
                'weight',
                'string',
                ['oro_options' => [
                    'extend' => [
                        'is_extend' => true,
                        'owner' => ExtendScope::OWNER_CUSTOM,
                        'cascade' => ['persist', 'remove'],
                        'on_delete' => 'CASCADE',
                    ],
                    'entity' => ['label' => 'Weight'],
                    'datagrid' => ['is_visible' => false],
                ]]
            );
        }

        if (!$table->hasColumn('weightUom')) {
            $table->addColumn(
                'weightUom',
                'string',
                ['oro_options' => [
                    'extend' => [
                        'is_extend' => true,
                        'owner' => ExtendScope::OWNER_CUSTOM,
                        'cascade' => ['persist', 'remove'],
                        'on_delete' => 'CASCADE',
                    ],
                    'entity' => ['label' => 'Weight unit of measurement'],
                    'datagrid' => ['is_visible' => false],
                ]]
            );
        }
    }

    /**
     * {@inheritDoc}
     */
    public function setActivityExtension(ActivityExtension $activityExtension)
    {
        $this->activityExtension = $activityExtension;
    }
}
