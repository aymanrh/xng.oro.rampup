<?php

declare(strict_types=1);

namespace Xngage\ProductBundle\Command;

use Doctrine\ORM\EntityManager;
use Http\Client\Common\HttpMethodsClientInterface;
use Oro\Bundle\CatalogBundle\Entity\Category;
use Oro\Bundle\CatalogBundle\Entity\CategoryTitle;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\CronBundle\Command\CronCommandInterface;
use Oro\Bundle\ProductBundle\Entity\ProductImageType;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Xngage\ProductBundle\Api\CoreOroApi;
use Xngage\ProductBundle\DependencyInjection\Configuration;

class SyncProductsCommand extends Command implements CronCommandInterface
{
    /** @var string */
    protected static $defaultName = 'oro:cron:xngage-sync-product';

    const API_ENDPOINT = 'http://foodmaven-dev.xngage.net/product.php';
    const ADDED = 'added';
    const UPDATED = 'updated';

    private ConfigManager $configManager;
    private HttpMethodsClientInterface $curlClient;
    private EntityManager $entityManager;
    private CoreOroApi $oroApi;

    public function __construct(
        ConfigManager $configManager,
        HttpMethodsClientInterface $curlClient,
        EntityManager $entityManager,
        CoreOroApi $coreOroApi
    ) {
        $this->configManager = $configManager;
        $this->curlClient = $curlClient;
        $this->entityManager = $entityManager;
        $this->oroApi = $coreOroApi;

        parent::__construct();
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaultDefinition()
    {
        return $this->configManager->get(Configuration::ROOT_NODE.'.'.Configuration::PRODUCTS_SYNC_CRON_SCHEDULE);
    }

    /**
     * {@inheritDoc}
     */
    public function isActive()
    {
        return true;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($products = $this->fetchProductsFromERP()) {
            $output->writeln('<info>Products have been loaded successfully from the ERP</info>');
            foreach ($products as $product) {
                try {
                    $result = $this->addOrUpdateProduct($product);
                    $output->writeln($result->getStatusCode());
                } catch (ORMException $e) {
                    $output->writeln("<error>{$e->getMessage()}</error>");
                }
            }
        } else {
            $output->writeln('<error>The request has been failed</error>');
        }
    }

    private function fetchProductsFromERP()
    {
        $response = $this->curlClient->get(self::API_ENDPOINT);
        if (Response::HTTP_OK === $response->getStatusCode()) {
            return json_decode((string) $response->getBody(), true);
        }

        return null;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    protected function addOrUpdateProduct(array $recordData)
    {
        if (!empty($recordData['Sku']['value'])) {
            $product = [
                'data' => [
                    'type' => 'products',
                    'attributes' => [
                        'sku' => $recordData['Sku']['value'],
                        'status' => 'Active' === $recordData['ItemStatus']['value'] ? 'enabled' : 'disabled',
                        'productType' => 'simple',
                        'featured' => false,
                        'newArrival' => false,
                        'availability_date' => null,
                        'volume' => $recordData['Volume']['value'],
                        'weight' => $recordData['Weight']['value'],
                        'volumeUom' => $recordData['volumeUom']['value'],
                        'weightUom' => $recordData['weightUom']['value'],
                    ],
                    'relationships' => [],
                ],
                'included' => [],
            ];

            $this->addName($product, $recordData['Name']['value']);
            $this->addAttributeFamily($product, '1');
            $this->addDescription($product, $recordData['Description']['value']);
            $this->addUnitPrecisions($product);
            $this->addPrimaryUnitPrecision($product);
            $this->addImages($product, $recordData['Images']);
            $this->addInventoryStatus($product, 'in_stock');
            foreach ($recordData['Category'] as $category) {
                $this->addCategory($product, $category);
            }
            foreach ($recordData['VendorDetails'] as $brand) {
                $this->addBrand($product, $brand);
            }

            return $this->oroApi->callApi(Request::METHOD_POST, 'products', [], $product);
        }

        return null;
    }

    protected function addName(&$productArray, $name)
    {
        $relationshipName = 'names';
        $relationshipRecord = ['type' => 'productnames', 'id' => $nameId = uniqid('name')];
        $includedRecord = [
            'type' => 'productnames',
            'id' => $nameId,
            'attributes' => [
                'fallback' => null,
                'string' => $name,
            ],
            'relationships' => [
                'localization' => [
                    'data' => null,
                ],
            ],
        ];

        $this->addIncludedData($productArray, $includedRecord);
        $this->addRelationshipData($productArray, $relationshipName, $relationshipRecord, true);
    }

    protected function addAttributeFamily(&$productArray, $attributeFamilyId)
    {
        $relationshipName = 'attributeFamily';
        $relationshipRecord = ['type' => 'attributefamilies', 'id' => $attributeFamilyId];

        $this->addRelationshipData($productArray, $relationshipName, $relationshipRecord);
    }

    protected function addShortDescription(&$productArray, $shortDescription)
    {
        $relationshipName = 'productshortdescriptions';
        $relationshipRecord = [
            'type' => 'productshortdescriptions',
            'id' => $shortDescriptionId = uniqid('productshortdescription'),
        ];
        $includedRecord = [
            'type' => 'productshortdescriptions',
            'id' => $shortDescriptionId,
            'attributes' => [
                'fallback' => null,
                'text' => $shortDescription,
            ],
        ];

        $this->addIncludedData($productArray, $includedRecord);
        $this->addRelationshipData($productArray, $relationshipName, $relationshipRecord);
    }

    protected function addDescription(&$productArray, $description)
    {
        $relationshipName = 'descriptions';
        $relationshipRecord = ['type' => 'productdescriptions', 'id' => $descId = 'description-1'];
        $includedRecord = [
            'type' => 'productdescriptions',
            'id' => $descId,
            'attributes' => [
                'fallback' => null,
                'wysiwyg' => [
                    'value' => strip_tags($description),
                    'valueRendered' => $description,
                    'style' => null,
                    'properties' => null,
                ],
            ],
            'relationships' => [
                'localization' => [
                    'data' => null,
                ],
            ],
        ];

        $this->addIncludedData($productArray, $includedRecord);
        $this->addRelationshipData($productArray, $relationshipName, $relationshipRecord, true);
    }

    protected function addUnitPrecisions(&$productArray)
    {
        $relationshipName = 'unitPrecisions';
        $relationshipRecord = ['type' => 'productunitprecisions', 'id' => $pupid = uniqid('product-unit-precision-id-')];
        $includedRecord = [
            'type' => 'productunitprecisions',
            'id' => $pupid,
            'attributes' => [
                'precision' => '0',
                'conversionRate' => '5',
                'sell' => true,
            ],
            'relationships' => [
                'unit' => [
                    'data' => [
                        'type' => 'productunits',
                        'id' => 'each',
                    ],
                ],
            ],
        ];

        $this->addIncludedData($productArray, $includedRecord);
        $this->addRelationshipData($productArray, $relationshipName, $relationshipRecord, true);
    }

    protected function addPrimaryUnitPrecision(&$productArray)
    {
        $relationshipName = 'primaryUnitPrecision';
        $relationshipRecord = ['type' => 'productunitprecisions', 'id' => $pupid = uniqid('product-unit-precision-id-')];
        $includedRecord = [
            'type' => 'productunitprecisions',
            'id' => $pupid,
            'attributes' => [
                'precision' => '0',
                'conversionRate' => '2',
                'sell' => true,
            ],
            'relationships' => [
                'unit' => [
                    'data' => [
                        'type' => 'productunits',
                        'id' => 'set',
                    ],
                ],
            ],
        ];

        $this->addIncludedData($productArray, $includedRecord);
        $this->addRelationshipData($productArray, $relationshipName, $relationshipRecord);
    }

    protected function addBrand(&$productArray, $brand)
    {
        $relationshipName = 'brand';
        $relationshipRecord = ['type' => 'brands', 'id' => $brand['VendorID']['value']];
        $includedRecord = [
            'type' => 'brands',
            'id' => $brand['VendorID']['value'],
            'attributes' => [
                'status' => 'enabled',
            ],
            'relationships' => [
                'names' => [
                    'data' => [
                        [
                            'type' => 'localizedfallbackvalues',
                            'id' => $brandNameId = uniqid('brandname-'),
                        ],
                    ],
                ],
            ],
        ];
        $includedBrandName = [
            'type' => 'localizedfallbackvalues',
            'id' => $brandNameId,
            'attributes' => [
                'fallback' => null,
                'string' => $brand['VendorName']['value'],
                'text' => null,
            ],
            'relationships' => [
                'localization' => [
                    'data' => null,
                ],
            ],
        ];

        $this->addIncludedData($productArray, $includedRecord);
        $this->addIncludedData($productArray, $includedBrandName);
        $this->addRelationshipData($productArray, $relationshipName, $relationshipRecord);
    }

    protected function addImages(&$productArray, $images)
    {
        $productRepository = $this->entityManager->getRepository('OroProductBundle:Product');
        $lastProductId = $productRepository->createQueryBuilder('p')
            ->select('p.id')
            ->addOrderBy('p.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()->getResult()[0]['id'];

        if (empty($images)) {
            return;
        }

        usort($images, function ($image1, $image2) {
            return $image1['sort'] <=> $image2['sort'];
        });

        foreach ($images as $index => $image) {
            $relationshipName = 'images';
            $imageType = ProductImageType::TYPE_ADDITIONAL;
            if (0 === $index) {
                $imageType = ProductImageType::TYPE_MAIN;
            }

            $relationshipRecord = ['type' => 'productimages', 'id' => $imageId = uniqid('product-image')];
            $this->addImage($productArray, $image['fileName'], $imageId, $imageType, $lastProductId);
            $this->addRelationshipData($productArray, $relationshipName, $relationshipRecord, true);
        }
    }

    private function addImage(&$productArray, $imageUrl, $imageId, $imageType, $productId)
    {
        $files = [
            'type' => 'files',
            'id' => $fileId = uniqid('file-'),
            'attributes' => [
                'mimeType' => getimagesize($imageUrl)['mime'],
                'originalFilename' => basename($imageUrl),
                'fileSize' => strlen(file_get_contents($imageUrl)),
                'content' => base64_encode(file_get_contents($imageUrl)),
            ],
        ];

        $productImageTypes = [
            'type' => 'productimagetypes',
            'id' => $productImageTypeId = uniqid('product-image-type-'),
            'attributes' => [
                'productImageTypeType' => $imageType,
            ],
            'relationships' => [
                'productImage' => [
                    'data' => [
                        'type' => 'productimages',
                        'id' => $imageId,
                    ],
                ],
            ],
        ];

        $productImages = [
            'type' => 'productimages',
            'id' => $imageId,
            'relationships' => [
                'image' => [
                    'data' => [
                        'type' => 'files',
                        'id' => $fileId,
                    ],
                ],
                'types' => [
                    'data' => [
                        [
                            'type' => 'productimagetypes',
                            'id' => $productImageTypeId,
                        ],
                    ],
                ],
                'product' => [
                    'data' => [
                        'type' => 'products',
                        'id' => (string) $productId,
                    ],
                ],
            ],
        ];

        $this->addIncludedData($productArray, $files);
        $this->addIncludedData($productArray, $productImageTypes);
        $this->addIncludedData($productArray, $productImages);
    }

    protected function addCategory(&$productArray, $category)
    {
        $categoryErpId = $category['CategoryID']['value'];
        $categoryRepository = $this->entityManager->getRepository('OroCatalogBundle:Category');
        $categoryObj = $categoryRepository->findOneBy(['erp_id' => $categoryErpId]);
        if (!$categoryObj) {
            $categoryObj = new Category();
            $categoryObj->setErpId($categoryErpId);
        }

        if (!empty($category['ParentCategoryID']['value']) &&
            $parentCategory = $categoryRepository->findOneBy(['erp_id' => $category['ParentCategoryID']['value']])) {
            $categoryObj->setParentCategory($parentCategory);
        }

        $categoryTitle = (new CategoryTitle())->setString($category['CategoryName']['value']);
        $categoryObj->addTitle($categoryTitle);
        $this->entityManager->persist($categoryTitle);
        $this->entityManager->persist($categoryObj);
        $this->entityManager->flush();

        $relationshipName = 'category';
        $relationshipRecord = ['type' => 'categories', 'id' => (string) $categoryObj->getId()];
        $this->addRelationshipData($productArray, $relationshipName, $relationshipRecord);
    }

    protected function addInventoryStatus(&$productArray, $status)
    {
        $relationshipName = 'inventory_status';
        $relationshipRecord = [
            'type' => 'prodinventorystatuses',
            'id' => $status,
        ];

        $this->addRelationshipData($productArray, $relationshipName, $relationshipRecord);
    }

    private function addRelationshipData(
        &$productArray,
        $relationshipName,
        $relationshipRecord,
        $pluralRelationshipEntity = false
    ) {
        if (!isset($productArray['data']['relationships'][$relationshipName]['data'])) {
            if ($pluralRelationshipEntity) {
                $_data = [];
                array_push($_data, $relationshipRecord);
            } else {
                $_data = $relationshipRecord;
            }
            $productArray['data']['relationships'][$relationshipName]['data'] = $_data;

            return;
        }

        $_data = $productArray['data']['relationships'][$relationshipName]['data'];
        if (!$pluralRelationshipEntity && !isset($_data[0])) {
            $_oldData = $_data;
            $_data = [];
            array_push($_data, $_oldData);
        }

        array_push($_data, $relationshipRecord);
        $productArray['data']['relationships'][$relationshipName]['data'] = $_data;
    }

    private function addIncludedData(&$productArray, $includedRecord)
    {
        array_push($productArray['included'], $includedRecord);
    }
}
